This folder contains Game Builds, this is what you use to start the game.

If you want to play the game and don't care about the developer side, just keep the build with the latest version number, and delete the rest (except maybe the rules book if you need it).

# Splendor on Unity

## Description

This Project is a **Unity** version of the **Board Game Splendor**, in which you play as a merchant guild and buy mines that produce resources to buy even more mines!
This Project Started as a school Project.

## How to Use

*Warning: some instructions consider the game as a final, working product, which isn't the case right now, so some details might not be true yet.*

### Game Rules

You can find the original game rules inside **documentation/rules**, in both english and french.

### Start the Game

Start by opening the **builds** folder and run the most recent build, twice. Each player will use an instance of the game.
Now there should be buttons in a corner of the window. On one of them is written "Host (Server + Client)". Click on that button with one of the two applications. We will call this player "A". After that, the other player, "B", should be able to connect to the game simply by pressing the "Client" button.

~~When two player are connected, the "Start Game" button will appear. press it.~~ (not done yet)

### User Interface

When the game starts, a lot of things appear.

On the top left is your player area, displaying how many mines of each color you have in the rectangles, listing your tokens, and telling your current score, in yellow.

Under that is a similar area, displaying your opponent's status.

~~Then you have a **Yellow Token Pile** and **Three card Slot**s. Those are your **reserved cards**, that only you can see and buy. The token pile is the **Joker Pile (Gold)**~~ for now this hasn't been implemented.

At the bottom you also have the list of **regular tokens**, with buttons above them to select how many of each to pick up, and a validation button (not yet).

On the right side is the game board, from bottom to top: 4 Tier 1 Mines, 4 Tier 2 Mines, 4 Tier 3 Mines, and finally the Nobles.
The higher the tier, the higher the score gained, but so is the cost!

The cost of a **mine** is indicated with the circles: the color indicates what resource is concerned, and the number inside it tells how much of that resource is needed. The global color of the card is what it'll produce. The Number at the top of the card is the points that a player will gain for buying it, if there's no number, then the card will give none.

As for the **Nobles**, they give no resources, so they all are grey. The resource cost is now given with rectangles, because you need to produce them via mines only, tokens aren't considered.

~~There is also a "Pass Turn" Button.~~ not yet, for now it's a "Generate Cards" one instead, used to start the game.

### How to Play

Each player will take a turn (starting with "A"), where they can make only one action amongst the following:

    - Take 3 Tokens of Different Colors
        - Done by using the arrow buttons above the token piles.
        - You obviously cannot pick a token if the pile is empty.
        - It is possible to take less than 3 tokens.
        - The action must be validated by a button when you are ready to pick them up.
    - Take 2 Tokens of the Same Color
        - Done by using the arrow buttons above the token piles.
        - The pile must have at least 4 tokens to pick 2 of them.
        - It is possible to take only one token.
        - The action must be validated by a button when you are ready to pick them up.
    - Buy a Mine
        - Done by dragging it over your own player area.
        - If you have enough resources to buy it:
            - It will remove some tokens from you if needed and return them to their pile on the board
            - Your production count of the corresponding color will be incremented
            - The mine's points value will be added to your score
            - The mine will then disappear
            - if it doesn't come from your own reserved cards, it'll be replaced by another one of the same tier
    - Reserve a Mine
        - Done by dragging it over the reserved mines area.
        - If your area is full (3 slots), it will go back to where it was.
        - Else:
            - It will go in an area where only you can see and buy it
            - You will receive 1 Gold, the Yellow Token that serves as a Joker (can be used as a replacement for any color)
        - It's the only way to get Yellow Tokens.
    - Pass Turn
        - By clicking on the "Pass Turn Button", you will do nothing and end the turn.

When trying to buy a Mine, the game will use your personal mines production in priority, then the normal tokens, and finally the Gold Tokens if needed.

**Arrows** above token piles will be **greyed-out** if they cannot be used (following "1 of 3 colors that have at least 1 remaining" or "2 of one colors that have at least 4 remaining" conditions).

If you have **more than 10 tokens** at the end of your turn, you'll have to choose which ones you'll **put back** before the turn actually ends.

Just before the **end of each turn** you play, the game will also check if you have enough mines for a **Noble** to join your side. If you are the first to reach the **requirements**, the noble will **disappear** and their **score** will be given to you directly.

When a player reaches **15 points**, the game only ends after **finishing the round**. The board will then reset, everything will disappear and the "**Start Game**" button will reappear.

### Notes

For now, the game is a local **2 Players Versus** game (AIs and Online play will be implemented in the future) and some features are broken, making the game unplayable.

## Installation (for Developers)

### Unity Installation

First of all, please go on **unity3d.com/get-unity/downloads** to get **Unity Hub**. When Unity Hub is installed, open it and go to the installs tab. click install editor to choose a version of Unity to download. Only the most recent builds are displayed here, to get older Unity Builds, go to **unity3d.com/get-unity/download/archive** and search for the version you want.

The game was developed using **Unity 2020.3.29f1** (but it should work with later builds, too)

If you're using **Visual Studio**, you may want to install the Unity Tools Module Integration in the plugin selection.


## How to edit the Game via Unity?

Start **Unity Hub**, and launch the project.

Now, that you're on the **Unity Editor**, there are a lot of things that you wan **tweak** (you can change absolutely anything, actually, since we built everything from scratch anyway...)
If you're new to Unity, and want to know how to make a few basic changes, please go in **documentation/tutorials**, and read "**Unity Beginner's Guide & How to Edit the Project.pdf**", it's a tutorial for your first steps on Unity using this project as a sandbox to experiment on.

## To Do List

    - Mine System
        - Requirements Verification
        - Mine Buying Feedback
    - Noble System
        - Requirements Verification
        - Score Addition
        - Noble Destruction
        - Noble Joining Player Feedback
    - Token Picker
        - Token Count
        - Tokens objects in Pile
        - Token Appearing/Disappearing
        - Arrow Picker
        - Conditional Picks
        - Validation Button
    - Gold Token System
        - Cannot Pick Up
        - Serves as a Joker
    - Token Overload Management
        - Adding Tokens to the Player
        - Overload Display Solution
        - Tokens to Return Picker
        - Validation Button
    - Player Class
        - Score
        - Mine Production
        - Tokens List
        - Name
    - Multiplayer Synchronization
        - Mine Synchronization
        - Noble Synchronization
        - Player Display Synchronization
        - Invert Displays
        - Visual Updates
    - Turn System
        - Do Only One Action
        - Change Active Player
        - Stop Player Interaction if not Active
    - Pass Turn button
        - Button Design
        - Counts as an Action
    - Game Management
        - Start Method (Button)
        - Players List
        - Rounds Management
        - Turns Management
        - End Condition
        - Winner Feedback
        - Restart Option
    - Mine Reservation System
        - Reserve Area Design
        - Reservation System
        - Gold Token
    - 3 and More Players Integration
        - Change Opponent Display Method
        - Player List
        - Synchronization
        - Change Token Piles Sizes
        - Player Count Limit
    - AI Integration
        - Way for Host to add AIs
        - Develop Selection Logic
    - Online
        - Mirror Online Integration
        - Online Synchronization
        - Accounts System?
        - Ranking?

## Credits

Developped on **Unity** by *DENIS Théo and STREINGER Guillaume*
Multiplayer code based on the **Mirror** Package.
We do not own the original Board Game.
Buy Splendor here: https://www.amazon.com/Asmodee-SPL01-Splendor/dp/B00IZEUFIA/

using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MineT1Controller : CardController
{
    public Mine card;

    public TextMeshProUGUI resourceCostD;
    public Image resourceTypeD;
    public List<List<int>> patterns;

    /// <summary>
    /// Runs on object's initialization.
    /// </summary>
    private void Initialize()
    {
        GenerateCard();
    }

    private void GenerateCardVariable()
    {
        card = new Mine(GetRandomPattern());
    }

    private void Awake()
    {
        SetPatterns();
        Initialize();
    }

    public void GenerateCard()
    {
        GenerateCardVariable();

        if (card.Score == 0)
            score.color = new Color32(0, 0, 0, 0);
        else
        {
            score.color = new Color32(255, 255, 255, 255);
            score.text = card.Score.ToString();
        }

        GetComponent<Image>().color = Colors.ResourceColor(card.MineType);

        resourceCostA.text = card.CostValueA.ToString();
        resourceTypeA.color = Colors.ResourceColor(card.CostTypeA);

        if (card.CostValueB == 0)
        {
            resourceCostB.color = new Color32(0, 0, 0, 0);
            resourceTypeB.color = new Color32(0, 0, 0, 0);
        }
        else
        {
            resourceCostB.color = new Color32(255, 255, 255, 255);
            resourceCostB.text = card.CostValueB.ToString();
            resourceTypeB.color = Colors.ResourceColor(card.CostTypeB);
        }

        if (card.CostValueC == 0)
        {
            resourceCostC.color = new Color32(0, 0, 0, 0);
            resourceTypeC.color = new Color32(0, 0, 0, 0);
        }
        else
        {
            resourceCostC.color = new Color32(255, 255, 255, 255);
            resourceCostC.text = card.CostValueC.ToString();
            resourceTypeC.color = Colors.ResourceColor(card.CostTypeC);
        }

        if (card.CostValueD == 0)
        {
            resourceCostD.color = new Color32(0, 0, 0, 0);
            resourceTypeD.color = new Color32(0, 0, 0, 0);
        }
        else
        {
            resourceCostD.color = new Color32(255, 255, 255, 255);
            resourceCostD.text = card.CostValueD.ToString();
            resourceTypeD.color = Colors.ResourceColor(card.CostTypeD);
        }
    }

    public List<int> GetRandomPattern()
    {
        return patterns[Random.Range(0, patterns.Count)];
    }

    public void SetPatterns()
    {
        patterns = new List<List<int>>();
        patterns.Add(new List<int> { 1, 4 });
        patterns.Add(new List<int> { 0, 3 });
        patterns.Add(new List<int> { 0, 2, 1 });
        patterns.Add(new List<int> { 0, 2, 2 });
        patterns.Add(new List<int> { 0, 2, 2, 1 });
        patterns.Add(new List<int> { 0, 1, 1, 3 });
        patterns.Add(new List<int> { 0, 1, 1, 1, 1 });
        patterns.Add(new List<int> { 0, 1, 1, 1, 2 });
    }
}
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TokenPileController : MonoBehaviour
{
    private TokenPile tokens;
    public TextMeshProUGUI count;

    private void Awake()
    {
        tokens = new TokenPile();
        Initialize(ref tokens);
    }

    // Start is called before the first frame update
    private void Start()
    {
        
    }

    // Update is called once per frame
    private void Update()
    {
        
    }

    private void Initialize(ref TokenPile token)
    {
        count.text = token.getCount().ToString();
    }
}

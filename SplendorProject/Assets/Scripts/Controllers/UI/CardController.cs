using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CardController : MonoBehaviour
{
    public TextMeshProUGUI score;
    public TextMeshProUGUI resourceCostA, resourceCostB, resourceCostC;
    public Image resourceTypeA, resourceTypeB, resourceTypeC;
}

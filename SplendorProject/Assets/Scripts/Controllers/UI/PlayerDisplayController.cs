using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

[Serializable]

public class PlayerDisplayController : MonoBehaviour
{
    public TextMeshProUGUI Score;
    public TextMeshProUGUI ResourceProductionA;
    public TextMeshProUGUI ResourceProductionB;
    public TextMeshProUGUI ResourceProductionC;
    public TextMeshProUGUI ResourceProductionD;
    public TextMeshProUGUI ResourceProductionE;
    public Image[] ResourceTypes;
    public Image[] Tokens;

    private void Awake()
    {
        Score.text = "0";

        ResourceTypes[0].color = Colors.ResourceColor(1);
        ResourceProductionA.text = "0";
        ResourceTypes[1].color = Colors.ResourceColor(2);
        ResourceProductionB.text = "0";
        ResourceTypes[2].color = Colors.ResourceColor(3);
        ResourceProductionC.text = "0";
        ResourceTypes[3].color = Colors.ResourceColor(4);
        ResourceProductionD.text = "0";
        ResourceTypes[4].color = Colors.ResourceColor(5);
        ResourceProductionE.text = "0";
    }

    public void AddProduction(int type)
    {
        int total = 0;

        if (type == 1)
        {
            int.TryParse(ResourceProductionA.GetParsedText(), out total);
            total++;
            ResourceProductionA.text = total.ToString();
        }
        else if (type == 2)
        {
            int.TryParse(ResourceProductionB.GetParsedText(), out total);
            total++;
            ResourceProductionB.text = total.ToString();
        }
        else if (type == 3)
        {
            int.TryParse(ResourceProductionC.GetParsedText(), out total);
            total++;
            ResourceProductionC.text = total.ToString();
        }
        else if (type == 4)
        {
            int.TryParse(ResourceProductionD.GetParsedText(), out total);
            total++;
            ResourceProductionD.text = total.ToString();
        }
        else if (type == 5)
        {
            int.TryParse(ResourceProductionE.GetParsedText(), out total);
            total++;
            ResourceProductionE.text = total.ToString();
        }
    }

    public void AddScore(int score)
    {
        int total = 0;
        int.TryParse(Score.GetParsedText(), out total);
        total += score;

        Score.text = total.ToString();
    }
}

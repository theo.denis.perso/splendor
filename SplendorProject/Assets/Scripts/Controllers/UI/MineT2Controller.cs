using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MineT2Controller : CardController
{
    public Mine card;

    public TextMeshProUGUI resourceCostD;
    public Image resourceTypeD;
    public List<List<int>> patterns;

    /// <summary>
    /// Runs on object's initialization.
    /// </summary>
    private void Initialize()
    {
        GenerateCard();
    }

    private void GenerateCardVariable()
    {
        card = new Mine(GetRandomPattern());
    }

    private void Awake()
    {
        GetPatterns();
        Initialize();
    }

    public void GenerateCard()
    {
        GenerateCardVariable();

        score.text = card.Score.ToString();

        GetComponent<Image>().color = Colors.ResourceColor(card.MineType);

        if (card.CostValueA == 0)
        {
            resourceCostA.color = new Color32(0, 0, 0, 0);
            resourceTypeA.color = new Color32(0, 0, 0, 0);
        }
        else
        {
            resourceCostA.color = new Color32(255, 255, 255, 255);
            resourceCostA.text = card.CostValueA.ToString();
            resourceTypeA.color = Colors.ResourceColor(card.CostTypeA);
        }

        if (card.CostValueB == 0)
        {
            resourceCostB.color = new Color32(0, 0, 0, 0);
            resourceTypeB.color = new Color32(0, 0, 0, 0);
        }
        else
        {
            resourceCostB.color = new Color32(255, 255, 255, 255);
            resourceCostB.text = card.CostValueB.ToString();
            resourceTypeB.color = Colors.ResourceColor(card.CostTypeB);
        }

        if (card.CostValueC == 0)
        {
            resourceCostC.color = new Color32(0, 0, 0, 0);
            resourceTypeC.color = new Color32(0, 0, 0, 0);
        }
        else
        {
            resourceCostC.color = new Color32(255, 255, 255, 255);
            resourceCostC.text = card.CostValueC.ToString();
            resourceTypeC.color = Colors.ResourceColor(card.CostTypeC);
        }

        if (card.CostValueD == 0)
        {
            resourceCostD.color = new Color32(0, 0, 0, 0);
            resourceTypeD.color = new Color32(0, 0, 0, 0);
        }
        else
        {
            resourceCostD.color = new Color32(255, 255, 255, 255);
            resourceCostD.text = card.CostValueD.ToString();
            resourceTypeD.color = Colors.ResourceColor(card.CostTypeD);
        }
    }

        public List<int> GetRandomPattern()
    {
        return patterns[Random.Range(0, patterns.Count)];
    }

    public void GetPatterns()
    {
        patterns = new List<List<int>>();
        patterns.Add(new List<int> {2, 5});
        patterns.Add(new List<int> {3, 6});
        patterns.Add(new List<int> {2, 5, 3});
        patterns.Add(new List<int> {1, 3, 3, 2});
        patterns.Add(new List<int> {1, 2, 2, 3});
        patterns.Add(new List<int> {2, 4, 2, 1});
    }
}
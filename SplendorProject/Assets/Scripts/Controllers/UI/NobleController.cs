using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class NobleController : CardController
{
    public Noble card;
    
    public List<List<int>> patterns;

    private void Awake()
    {
        GetPatterns();
        Initialize();
    }

    /// <summary>
    /// Runs on object's initialization.
    /// </summary>
    private void Initialize()
    {
        GenerateCard();
    }

    public void GenerateCard()
    {
        card = new Noble(GetRandomPattern());
        
        score.text = card.Score.ToString();
        
        resourceCostA.text = card.CostValueA.ToString();
        resourceTypeA.color = Colors.ResourceColor(card.CostTypeA);

        if (card.CostValueB == 0)
        {
            resourceCostB.color = new Color32(0, 0, 0, 0);
            resourceTypeB.color = new Color32(0, 0, 0, 0);
        }
        else
        {
            resourceCostB.color = new Color32(255, 255, 255, 255);
            resourceCostB.text = card.CostValueB.ToString();
            resourceTypeB.color = Colors.ResourceColor(card.CostTypeB);
        }

        if (card.CostValueC == 0)
        {
            resourceCostC.color = new Color32(0, 0, 0, 0);
            resourceTypeC.color = new Color32(0, 0, 0, 0);
        }
        else
        {
            resourceCostC.color = new Color32(255, 255, 255, 255);
            resourceCostC.text = card.CostValueC.ToString();
            resourceTypeC.color = Colors.ResourceColor(card.CostTypeC);
        }

    }

    public void GetPatterns()
    {
        patterns = new List<List<int>>();
        patterns.Add(new List<int> {3, 4, 4});
        patterns.Add(new List<int> {3, 3, 3, 3});
    }

    public List<int> GetRandomPattern()
    {
        return patterns[Random.Range(0, patterns.Count)];
    }
}

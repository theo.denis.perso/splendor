using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public abstract class MineController : CardController
{
    public Mine card;
    
    public TextMeshProUGUI resourceCostD;
    public Image resourceTypeD;
    public List<List<int>> patterns;

    /// <summary>
    /// Runs on object's initialization.
    /// </summary>
    private void Initialize()
    {
        GenerateCard();
    }

    public void GenerateCard()
    {
        GenerateCardVariable();

        score.text = card.Score.ToString();
        resourceCostA.text = card.CostValueA.ToString();
        resourceCostB.text = card.CostValueB.ToString();
        resourceCostC.text = card.CostValueC.ToString();
        resourceCostD.text = card.CostValueD.ToString();

        GetComponent<Image>().color = Colors.ResourceColor(card.MineType);

        resourceTypeA.color = Colors.ResourceColor(card.CostTypeA);
        resourceTypeB.color = Colors.ResourceColor(card.CostTypeB);
        resourceTypeC.color = Colors.ResourceColor(card.CostTypeC);
        resourceTypeD.color = Colors.ResourceColor(card.CostTypeD);
    }

    private void GenerateCardVariable() { }
}

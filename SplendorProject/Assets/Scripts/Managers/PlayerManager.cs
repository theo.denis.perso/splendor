using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class PlayerManager : NetworkBehaviour
{
    public GameObject PlayerDisplay;
    public GameObject OpponentDisplay;

    public override void OnStartClient()
    {
        base.OnStartClient();

        PlayerDisplay = GameObject.Find("Player Display");
        OpponentDisplay = GameObject.Find("Opponent Display");
    }


}

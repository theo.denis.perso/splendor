using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]

public class GenerateCards : MonoBehaviour
{
    public GameObject Noble;
    public GameObject MineT1;
    public GameObject MineT2;
    public GameObject MineT3;
    public GameObject NobleArea;
    public GameObject MineT1Area;
    public GameObject MineT2Area;
    public GameObject MineT3Area;

    public void OnClick()
    {
        for (int i = 0; i < 4; i++)
        {
            // Converts i into the x offset of the card in its parent object
            // int x = 105 * (i * 2 - 3); // -315, -105, 105, 315

            if (i < 3)
            {
                GameObject n = Instantiate(Noble, new Vector2(0, 0), Quaternion.identity);
                n.transform.SetParent(NobleArea.transform, false);
            }

            GameObject t3 = Instantiate(MineT3, new Vector2(0, 0), Quaternion.identity);
            t3.transform.SetParent(MineT3Area.transform, false);

            GameObject t2 = Instantiate(MineT2, new Vector2(0, 0), Quaternion.identity);
            t2.transform.SetParent(MineT2Area.transform, false);

            GameObject t1 = Instantiate(MineT1, new Vector2(0, 0), Quaternion.identity);
            t1.transform.SetParent(MineT1Area.transform, false);
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardGenerator : MonoBehaviour
{
    public GameObject Card;
    public GameObject CardArea;

    public void Generate()
    {
        GameObject generated = Instantiate(Card, new Vector2(0, 0), Quaternion.identity);
        generated.transform.SetParent(CardArea.transform, false);
    }
}

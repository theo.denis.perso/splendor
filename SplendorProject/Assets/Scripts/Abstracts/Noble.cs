using System.Collections;
using System.Collections.Generic;
using UnityEngine.Assertions;
using Random = UnityEngine.Random;

[System.Serializable]

public class Noble : Card
{
    // 0 : (Gold, reserved for Tokens)
    // 1 : Red
    // 2 : Green
    // 3 : Blue
    // 4 : White
    // 5 : Black

    public Noble(List<int> pattern)
    {
        // must contain at least 1 value for the score and one for the cost
        Assert.IsTrue(pattern.Count >= 2);

        Score = pattern[0];

        // Checks that the score is positive, just in case
        if (Score < 0)
            Score = 0;

        List<int> unused = new List<int> {1, 2, 3, 4, 5};

        for (int i= 1; i<pattern.Count; i++)
        {
            int typeID;

            do
            {
                typeID = Random.Range(1, 6); // Between 1 and 5

                if (unused.Contains(typeID))
                {
                    if (i == 1)
                    {
                        CostTypeA = typeID;
                        CostValueA = pattern[1];
                    }
                    else if (i == 2)
                    {
                        CostTypeB = typeID;
                        CostValueB = pattern[2];
                    }
                    else if (i == 3)
                    {
                        CostTypeC = typeID;
                        CostValueC = pattern[3];
                    }
                }
            } while (!unused.Contains(typeID));

            unused.Remove(typeID);
        }
    }
}

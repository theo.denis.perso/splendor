using System.Collections;
using System.Collections.Generic;
using UnityEngine.Assertions;
using Random = UnityEngine.Random;

[System.Serializable]

public class Mine : Card
{
    public int CostValueD;
    public int CostTypeD { get; set; }
    public int MineType;

    // 0 : (Gold, reserved for Tokens)
    // 1 : Red
    // 2 : Green
    // 3 : Blue
    // 4 : White
    // 5 : Black

    // Pattern Example: { 0, 1, 1, 2} means a mine that costs 1 of a random color, 1 of another one, 2 of another one, and that gives 0 points when bought
    public Mine(List<int> pattern)
    {
        // must contain at least 1 value for the score and one for the cost
        Assert.IsTrue(pattern.Count >= 2);

        Score = pattern[0];

        List<int> unused = new List<int> { 1, 2, 3, 4, 5 };

        int count = 1;
        for (int index=1; index<pattern.Count; index++)
        {
            int typeID;

            do
            {
                typeID = Random.Range(1, 6); // Between 1 and 5

                if (unused.Contains(typeID))
                {
                    if (index == 1)
                    {
                        CostTypeA = typeID;
                        CostValueA = pattern[1];
                    }
                    else if (index == 2)
                    {
                        CostTypeB = typeID;
                        CostValueB = pattern[2];
                    }
                    else if (index == 3)
                    {
                        CostTypeC = typeID;
                        CostValueC = pattern[3];
                    }
                    else if (index == 4)
                    {
                        CostTypeD = typeID;
                        CostValueD = pattern[4];
                    }
                }
            } while(!unused.Contains(typeID));

            unused.Remove(typeID);
            count++;
        }

        for(int index=count+1; index<= 4; index++)
        {
            if (index == 2)
            {
                CostTypeB = -1;
                CostValueB = 0;
            }
            else if (index == 3)
            {
                CostTypeC = -1;
                CostValueC = 0;
            }
            else if (index == 4)
            {
                CostTypeD = -1;
                CostValueD = 0;
            }
        }

        MineType = Random.Range(1, 6); // Between 1 and 5
    }
}

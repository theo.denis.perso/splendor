using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]

public class TokenPile
{
    protected static int maxCount = 5;
    private int resourceID, count;

    /// <summary>
    /// Constructor. Creates a pile of Token with max count.
    /// </summary>
    public TokenPile()
    {
        count = maxCount;
    }

    /// <summary>
    /// Token Count getter
    /// </summary>
    /// 
    /// <returns>number of tokens in the pile</returns>
    public int getCount()
    {
        return count;
    }

    /// <summary>
    /// Constructor. Creates a pile of Token with custom count.
    /// </summary>
    /// 
    /// <param name="initialCount">Starting Count (will be re-set between upper and lower limits if exceeding)</param>
    public TokenPile(int initialCount)
    {
        // lower limit
        if (initialCount < 0)
            count = 0;
        // upper limit
        else if (initialCount <= maxCount)
            count = initialCount;
        else
            count = maxCount;
    }

    /// <summary>
    /// Adds multiple Token to the Pile.
    /// </summary>
    /// 
    /// <returns>True if all Tokens could be added and False otherwise</returns>
    public bool AddToken(int amount)
    {
        if(amount < 0)
            return false;

        if (count + amount <= maxCount)
        {
            count++;
            return true;
        }

        return false;
    }

    /// <summary>
    /// Adds a Token to the Pile.
    /// </summary>
    /// 
    /// <returns>True if a Token could be added and False otherwise</returns>
    public bool AddToken()
    {
        if (count + 1 <= maxCount)
        {
            count++;
            return true;
        }

        return false;
    }

    /// <summary>
    /// Removes a Token from the Pile.
    /// </summary>
    /// 
    /// <returns>True if a Token could be removed and False otherwise</returns>
    public bool RemoveToken()
    {
        if (count > 0)
        {
            count--;
            return true;
        }
        
        return false;
    }

    /// <summary>
    /// Token Resource ID getter
    /// </summary>
    /// 
    /// <returns>ID of the Resource of the Token</returns>
    public int getResourceID()
    {
        return resourceID;
    }
}

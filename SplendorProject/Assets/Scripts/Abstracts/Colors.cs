using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Colors
{
    private static Color32 GOLD = new Color32(255, 255, 16, 255); // Yellow
    private static Color32 RUBY = new Color32(128, 32, 32, 255); // Red
    private static Color32 EMERALD = new Color32(32, 128, 32, 255); // Green
    private static Color32 SAPPHIRE = new Color32(32, 32, 128, 255); // Blue
    private static Color32 QUARTZ = new Color32(208, 208, 208, 255); // White (Light Gray)
    private static Color32 ONYX = new Color32(48, 48, 48, 255); // Black

    /// <summary>
    /// Returns the Color32 corresponding to the corresponding resource
    /// </summary>
    /// 
    /// <param name="resourceID">ID of the resource (between 0 and 5)</param>
    /// 
    /// <returns>0-Yellow, 1-Red, 2-Green, 3-Blue, 4-White, 5-Black, other: Pure White</returns>
    public static Color32 ResourceColor(int resourceID)
    {
        return resourceID switch
        {
            0 => GOLD,
            1 => RUBY,
            2 => EMERALD,
            3 => SAPPHIRE,
            4 => QUARTZ,
            5 => ONYX,
            _ => new Color32(0, 0, 0, 0) // 100% Invisible (Black)
        };
    }
}
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using TMPro;
using UnityEngine;
using Vector2 = UnityEngine.Vector2;

public class DragAndDrop : MonoBehaviour
{
    public GameObject Canvas;
    public GameObject DropZone;
    private GameObject _dropZone;
    private GameObject _startParent;
    private bool _isDragged;
    private bool _isOverDropZone;
    private Vector2 _startPos;
    private Vector2 _dragOffset;

    // Start is called before the first frame update
    void Start()
    {
        Canvas = GameObject.Find("UI");
        DropZone = GameObject.Find("Player Display");
    }

    // Update is called once per frame
    void Update()
    {
        if (!_isDragged) return; // Optimization

        transform.position = new Vector2(Input.mousePosition.x + _dragOffset.x, Input.mousePosition.y + _dragOffset.y);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject == DropZone)
        {
            _isOverDropZone = true;
            _dropZone = collision.gameObject;
        }
    }

    private void OnCollisionExit2D(Collision2D _)
    {
        _isOverDropZone = false;
    }

    public void StartDrag()
    {
        _startParent = transform.parent.gameObject;
        _startPos = transform.position;
        _isDragged = true;
        _dragOffset = (Vector2) transform.position - (Vector2) Input.mousePosition;
        transform.SetParent(Canvas.transform, true);
    }

    public void EndDrag()
    {
        if (_isOverDropZone && true) // true needs to be replaced (later)
        {            
            Mine mine;

            try
            {
                mine = transform.gameObject.GetComponent<MineT1Controller>().card;
            }
            catch
            {
                try
                {
                    mine = transform.gameObject.GetComponent<MineT2Controller>().card;
                }
                catch
                {
                    mine = transform.gameObject.GetComponent<MineT3Controller>().card;
                }
            }

            _dropZone.GetComponent<PlayerDisplayController>().AddProduction(mine.MineType);
            _dropZone.GetComponent<PlayerDisplayController>().AddScore(mine.Score);

            Destroy(transform.gameObject);
        }
        else
        {
            transform.position = _startPos;
            transform.SetParent(_startParent.transform, false);
        }

        _isDragged = false;
    }
}

using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;

public class Cost
{
    // (resource ID, resource cost)
    private List<(int, int)> cost;

    /// <summary>
    /// Constructor.
    /// For example, a pattern of value {3, 5, 5} means the cost is:
    ///  - 3 of 1 resource,
    ///  - 5 of 2 different resources.
    ///  A resource can't be picked twice.
    /// </summary>
    /// 
    /// <param name="pattern">Cost Generation Pattern</param>
    public Cost(List<int> pattern)
    {
        List<int> resourcesID = new List<int>();
        int[] IDs = {1, 2, 3, 4, 5};
        resourcesID.AddRange(IDs);

        Random rand = new Random();

        for (int slot=0; slot<pattern.Count; slot++)
        {
            int id = rand.NextInt(resourcesID.Count);

            cost.Add((id, pattern[slot]));

            resourcesID.Remove(id);
        }
    }

    /// <summary>
    /// cost list value getter
    /// </summary>
    /// 
    /// <returns>(resouce ID, resource cost)</returns>
    public List<(int, int)> getCost()
    {
        return cost;
    }
}

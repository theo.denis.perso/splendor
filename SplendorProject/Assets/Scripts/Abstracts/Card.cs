using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]

public abstract class Card
{
    public int Score { get; set; }
    public int CostValueA, CostValueB, CostValueC;    
    
    public int CostTypeA { get; set; }
    public int CostTypeB { get; set; }
    public int CostTypeC { get; set; }
}
